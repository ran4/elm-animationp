module Main exposing (..)

--Imports {{{
-- import Html.Events

import AnimationFrame
import Html exposing (Html)
import Html.App as App
import Html.Attributes
import Svg
import Svg.Attributes exposing (x1, y1, x2, y2, cx, cy, r, fill, stroke, height)
import Time exposing (Time)
import Keyboard


main : Program Never
main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- }}}
-- MODEL {{{


type alias Debug =
    { lastKey : Keyboard.KeyCode
    , counter : Int
    }


type alias PlayingField =
    { w : Float
    , h : Float
    }


type alias Player =
    { x : Float
    , y : Float
    , radius : Float
    , direction : Float
    , speed : Float
    , turnSpeed : Float
    }


type alias Model =
    { player : Player
    , playingField : PlayingField
    , debug : Debug
    }


initPlayingField : PlayingField
initPlayingField =
    PlayingField 120 60


initPlayerX : Float
initPlayerX =
    initPlayingField.w / 2.0


initPlayerY : Float
initPlayerY =
    initPlayingField.h / 2.0


initPlayer : Player
initPlayer =
    let
        x =
            initPlayerX

        y =
            initPlayerY

        radius =
            5.0

        direction =
            0

        speed =
            -- 8.8 / 60.0
            2.8 / 60.0

        turnSpeed =
            0.3
    in
        Player x y radius direction speed turnSpeed


init : ( Model, Cmd Action )
init =
    ( Model initPlayer initPlayingField (Debug 0 0), Cmd.none )



--}}}
-- UPDATE {{{
    
within : (Float, Float) -> PlayingField -> Bool
within (x, y) playingField  =
    not (x < 0 || x > playingField.w || y < 0 || y > playingField.h)


updatePlayerPhysics : PlayingField -> Player -> Time -> Player
updatePlayerPhysics playingField player dt =
    let
        ( newX, newY, newDirection ) =
            if (player.x, player.y) `within` playingField == True then
                ( player.x + ((sin player.direction) * player.speed * dt)
                , player.y + ((cos player.direction) * player.speed * dt)
                , player.direction
                )
            else
                ( initPlayerX
                , initPlayerY
                , player.direction + 0.1
                )
    in
        { player
            | x = newX
            , y = newY
            , direction = newDirection
        }

drawEveryNthFrames : Int
drawEveryNthFrames =
    1

updateCounter : Debug -> Debug
updateCounter debug =
    { debug
        | counter =
            if debug.counter > drawEveryNthFrames then
                0
            else
                debug.counter + 1
    }


updateTime : Model -> Time -> Model
updateTime model dt =
    { model
        | player =
            updatePlayerPhysics
                model.playingField
                model.player
                (toFloat drawEveryNthFrames * dt)
        , debug = updateCounter model.debug
    }



-- model


updateDebug : Debug -> Keyboard.KeyCode -> Debug
updateDebug debug keyCode =
    { debug | lastKey = keyCode }


updatePlayerKeys : Player -> Keyboard.KeyCode -> Player
updatePlayerKeys player keyCode =
    let
        left =
            37

        up =
            38

        right =
            39

        down =
            40
    in
        if keyCode == left then
            { player | direction = player.direction + player.turnSpeed }
        else if keyCode == right then
            { player | direction = player.direction - player.turnSpeed }
        else if keyCode == up then
            player
        else if keyCode == down then
            player
        else
            player


type Action
    = NoOp
    | UpdateTime Time
    | KeyDownAction Keyboard.KeyCode


update : Action -> Model -> ( Model, Cmd Action )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        UpdateTime elapsedTime ->
            ( updateTime model elapsedTime, Cmd.none )

        KeyDownAction keyCode ->
            ( { model
                | debug = (updateDebug model.debug keyCode)
                , player = (updatePlayerKeys model.player keyCode)
              }
            , Cmd.none
            )



-- {model.debug | lastKey = keyCode}
--}}}
-- SUBSCRIPTIONS {{{


subscriptions : Model -> Sub Action
subscriptions model =
    Sub.batch
        [ AnimationFrame.diffs UpdateTime
        , Keyboard.downs KeyDownAction
        ]



--}}}
-- VIEW {{{


viewDebug : Model -> String
viewDebug model =
    (toString model.debug.lastKey)
        ++ (toString model.player.x)
        ++ ", "
        ++ (toString model.player.y)



--e.g. "0 0 100 100"


getPlayingFieldViewBox : PlayingField -> String
getPlayingFieldViewBox playingField =
    let
        wString =
            toString (floor playingField.w)

        hString =
            toString (floor playingField.h)
    in
        "0 0 " ++ wString ++ " " ++ hString


divStyle : Html.Attribute msg
divStyle =
    Html.Attributes.style
        [ ( "height", "96%" )
        , ( "border-style", "solid" )
        , ( "background", "#87cefa" )
        ]


view : Model -> Html Action
view model =
    Html.div [ divStyle ]
        [ Svg.svg
            [ Svg.Attributes.viewBox (getPlayingFieldViewBox model.playingField)
            , height "100%"
            ]
            [ Svg.circle
                [ cx (toString model.player.x)
                , cy (toString model.player.y)
                , r (toString model.player.radius)
                , fill "#0B79CE"
                ]
                []
            , let
                dx =
                    model.player.radius * (sin model.player.direction)

                dy =
                    model.player.radius * (cos model.player.direction)
              in
                Svg.line
                    [ x1 (toString model.player.x)
                    , y1 (toString model.player.y)
                    , x2 (toString (model.player.x + dx))
                    , y2 (toString (model.player.y + dy))
                    , stroke "#023963"
                    ]
                    []
            ]
          -- , Html.div [] [ Html.text (viewDebug model) ]
        , Html.div [] [ Html.text (viewDebug model) ]
        ]



--}}}
