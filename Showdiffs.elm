module Main exposing (..)

import Html exposing (Html)
import Html.App as App
import Html.Events
import AnimationFrame
import Time exposing (Time)


main : Program Never
main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { output : String
    , counter : Int}


init : ( Model, Cmd Action )
init =
    ( { output = "", counter = 0} , Cmd.none )



-- UPDATE


type Action
    = NoOp
    | Animate Time
    | UpdateCounter

updateOutput : Model -> Time -> String
updateOutput model elapsed =
    if model.counter < 60 then
        model.output ++ ", " ++ (toString elapsed)
    else
        model.output
        
incrementCounter : Model -> Model
incrementCounter model =
    { model | counter = model.counter + 1}

update : Action -> Model -> ( Model, Cmd Action )
update action model =
    case action of
        NoOp ->
            ( model, Cmd.none )
        Animate elapsed ->
            update UpdateCounter { model | output = (updateOutput model elapsed)}
        UpdateCounter ->
            ( incrementCounter model, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Action
subscriptions model =
    AnimationFrame.diffs Animate



-- VIEW


view : Model -> Html Action
view model =
    Html.div []
        [ Html.text model.output
        , Html.br [] []
        , Html.button [ Html.Events.onClick NoOp ] [ Html.text "Button text" ]
        ]
